 return (
		<div>
			<ReactTable
				data={makeData()}
				columns={columns}
				pivotBy={['firstName','lastName']}
				collapseOnDataChange={false}
				getTrProps={(state,rowInfo, column) => {
					return {
						onClick: e => {this.showDetailView(rowInfo)},
						style:{
							color: (rowInfo !== undefined && rowInfo.row !== null && rowInfo.row !== undefined && this.props.current !== null && rowInfo.row.firstName === this.props.current) ? '#E65100' : null
						}
					}
				}}
			/>
		</div>
	); 