export default class Price {
  constructor() {
    this.calcPrice = this.calcPrice.bind(this)
  }

  _getDiscountedPrice(
    itemPrice,
    numberOfItemsSold,
    {
      // defaults for no discount
      discountAmount = 0,
      isFixedDiscount = false,
      minItemsToBuy = 1,
    } = {} // Default for the destructuring
  ) {
    const discountIfPercentage = (
      discountAmount // I check if we have a discount
      && !isFixedDiscount // and if it's a fixed price discount, no pecentage
      && numberOfItemsSold >= minItemsToBuy // and enough items are bought
    ) ? (100 - discountAmount) / 100 // I return a decimal for the discount i.e. 0.70 for 70%
      : 1 // no discount

    const amountIfFixed = (numberOfItemsSold >= minItemsToBuy)
      ? discountAmount
      : itemPrice

    return isFixedDiscount
      ? amountIfFixed
      : itemPrice * discountIfPercentage
  }

  /**
   * I return the remaining items unnaffect by a get X for Y discount
   *
   * @param {*} numberOfItemsSold
   * @param {Object} discount { itemsToGet }
   * @returns
   * @memberof Price
   */
  _getItemsWithFullPrice(numberOfItemsSold, { itemsToGet }) {
    // I use the awesome modulo operator to get how many items the user
    // i.e. if you get 3 for the price of 1, but you bought 7 items, the 7th is full price
    return numberOfItemsSold % (itemsToGet || 1)
  }

  _getFreeFraction({ itemsToGet = 1, itemsToPayFor = 1 }) {
    // return what is actually actually pays for. I.e. 3 for 1 means only 1 is charged
    // so return 1/3

    return itemsToPayFor / itemsToGet
  }

  /**
   * Calcule the unit price of X amount of the same items
   *
   * @param {*} numberOfItemsSold number of items sold (these have to be of one SKU, IE, same item)
   * @param {*} itemPrice the unit price of the SKU
   * @param {number} [itemsToGet=1]
   * @param {number} [itemstoPayFor=1]
   * @param {number} [itemDiscount=0]
   * @param {number} [minItemsToBuy=1]
   * @returns
   * @memberof Price
   */
  calcPrice(
    numberOfItemsSold,
    itemPrice,
    discount = {},
  ) {
    const itemsUnaffectedByDiscount = this._getItemsWithFullPrice(numberOfItemsSold, discount)

    return this._getDiscountedPrice(
      itemPrice,
      numberOfItemsSold,
      discount,
    ) * ((
    // I calculate what portion of items are free (i.e. 1/3 when it's 3 for 1)
      this._getFreeFraction(discount)
      // multiple it with the number of chargeable items
      * (numberOfItemsSold - itemsUnaffectedByDiscount)
      // and then add the extra items that are charged fill price
    ) + this._getItemsWithFullPrice(numberOfItemsSold, discount))
  }

  /**
   * I return an array like so { 'classic': 3, 'premium': 1 }
   * where the value is the number of occurences
   *
   * @param {Array} items
   * @returns
   * @memberof Price
   */
  getItemCount = (items) => {
    const result = { }
    for (let i = 0; i < items.length; ++i) {
      if (!result[items[i]]) { result[items[i]] = 0 }
      ++result[items[i]]
    }

    return result
  }
}
