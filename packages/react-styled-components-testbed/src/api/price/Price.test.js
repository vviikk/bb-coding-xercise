import Price from './Price'

const price = new Price()

describe('Price', () => {
  it('should return 5 if itemPrice is 5 and 1 is bought', () => {
    expect(price.calcPrice(1, 5)).toEqual(5)
  })
  it('should return 10 if itemPrice is 5 and 2 are bought', () => {
    expect(price.calcPrice(2, 5)).toEqual(10)
  })
  it('should return 10 if itemPrice is 5 and 3 are bought, you get 2 for the price of 1', () => {
    expect(price.calcPrice(
      3, 5, { itemsToGet: 2, itemsToPayFor: 1 }
    )).toEqual(10)
  })
  it('should return 15 if itemPrice is 5 and 3 are bought', () => {
    expect(price.calcPrice(3, 5)).toEqual(15)
  })

  it(`should return 20 if itemPrice is 10 and 6 are bought,
    you get 3 for the price of 1`, () => {
    expect(price.calcPrice(
      6, 10, { itemsToGet: 3, itemsToPayFor: 1 }
    )).toEqual(20)
  })

  it(`should return 5 if itemPrice is 10 and 1 is bought
     and you get a discount of 50%`, () => {
    expect(price.calcPrice(
      1, 10, { discountAmount: 50 }
    )).toEqual(5)
  })

  it(`should return 10 if itemPrice is 10 and 6 are bought,
    you get 3 for the price of 1, and you get a 50% discount`, () => {
    expect(price.calcPrice(
      6, 10, { itemsToGet: 3, itemsToPayFor: 1, discountAmount: 50 }
    )).toEqual(10)
  })
  it(`should return 14 if itemPrice is 10 and 6 are bought,
     you get 3 for the price of 1, and you get a 30% discount`, () => {
    expect(price.calcPrice(
      6, 10, { itemsToGet: 3, itemsToPayFor: 1, discountAmount: 30 }
    )).toEqual(14)
  })

  it(`should return 15 if itemPrice is 15 and 1 is bought,
     and you get a 50% discount if you buy over 10 items`, () => {
    expect(price.calcPrice(
      1, 15, { discountAmount: 50, minItemsToBuy: 10 }
    )).toEqual(15)

    expect(price.calcPrice(
      10, 15, { discountAmount: 50, minItemsToBuy: 10 }
    )).toEqual(75)
  })

  it(`should return 20 if itemPrice is 10 and 6 are bought,
     you get 3 for the price of 1,
     and you get a 50% discount if you buy over 10 items`, () => {
    expect(price.calcPrice(
      6, 10, {
        itemsToGet: 3, itemsToPayFor: 1, discountAmount: 50, minItemsToBuy: 10,
      },
    )).toEqual(20)
  })
})

describe('count similar items properly', () => {
  it('should return the proper item count', () => {
    expect(price.getItemCount([1, 1, 2, 2, 3])).toEqual({ 1: 2, 2: 2, 3: 1 })
    expect(price.getItemCount(['classic', 'classic', 'standout'])).toEqual({ classic: 2, standout: 1 })
  })
})

describe('discount', () => {
  it('should return the price of item when discount is 0, or undefined', () => {
    expect(price._getDiscountedPrice(100, 100)).toEqual(100)
  })

  it('should return the half the price of item when discount is 50', () => {
    expect(price._getDiscountedPrice(
      100, 100, { discountAmount: 50 }
    )).toEqual(50)
  })
  it('should return the discounted price if it is set using isFixedDiscount', () => {
    expect(price._getDiscountedPrice(
      5, 100000, { discountAmount: 3, isFixedDiscount: true }
    )).toEqual(3)
  })

  it('should return 299.99 per add if discount price is implicitly set using isFixedDiscount', () => {
    expect(price._getDiscountedPrice(
      399.99,
      100,
      {
        user: 'apple',
        discountDescription: 'Get 3 for the price of 2 for Classic Ads',
        appliesTo: 'standout',
        discountAmount: 299.99,
        isFixedDiscount: true,
      }
    )).toEqual(299.99)
  })
  it(`should return 399.99 per add if discount
    price is implicitly set to 299.99,
    but you need 5 items or more for the discount to be triggered`, () => {
    const discount = {
      user: 'apple',
      discountDescription: 'Get 3 for the price of 2 for Classic Ads',
      appliesTo: 'standout',
      discountAmount: 299.99,
      isFixedDiscount: true,
      minItemsToBuy: 5,
    }
    expect(price._getDiscountedPrice(
      399.99,
      3,
      discount,
    )).toEqual(399.99)

    expect(price._getDiscountedPrice(
      399.99,
      4,
      discount,
    )).toEqual(399.99)

    expect(price._getDiscountedPrice(
      399.99,
      5,
      discount,
    )).toEqual(299.99)
  })
})

describe('calculate free items percentage', () => {
  it('should return 0.5 if you have a 2 for 1 offer', () => {
    const discount = { itemsToGet: 2, itemsToPayFor: 1 }
    expect(price._getFreeFraction(discount)).toEqual(0.5)
  })
  it('should return 1 if you dont have any offers', () => {
    const discount = {}
    expect(price._getFreeFraction(discount)).toEqual(1)
  })
})

describe('get remaining items not affected by offer', () => {
  it('should return 1 if you have a 2 for 1 offer and buy 3', () => {
    const discount = { itemsToGet: 2, itemsToPayFor: 1 }
    expect(price._getItemsWithFullPrice(3, discount)).toEqual(1)
  })
  it('should return 2 if you have a 3 for 2 offer and buy 8', () => {
    const discount = { itemsToGet: 3, itemsToPayFor: 2 }
    expect(price._getItemsWithFullPrice(8, discount)).toEqual(2)
  })

  it('should return 0 if you have a 5 for 2 offer and buy 10', () => {
    const discount = { itemsToGet: 5, itemsToPayFor: 2 }
    expect(price._getItemsWithFullPrice(10, discount)).toEqual(0)
  })

  it('should return 1 if you have a 5 for 2 offer and buy 11', () => {
    const discount = { itemsToGet: 5, itemsToPayFor: 2 }
    expect(price._getItemsWithFullPrice(11, discount)).toEqual(1.00)
  })
  // 10*(((11-1) * 2/5) + 1)
})
