import Checkout from './Checkout'

describe('checkout', () => {
  it('should initialize with zero items', () => {
    const checkout = new Checkout()
    expect(checkout.items).toHaveLength(0)
  })

  it('should have one item when item is added', () => {
    const checkout = new Checkout()
    checkout.addItem({ a: 'a' })
    expect(checkout.items).toHaveLength(1)
  })

  it(`should return 934.97 when 'classic', 'standout', 'premium'
    items added to the cart and user is unilever`, () => {
    const checkout = new Checkout({ username: 'unilever' })
    const items = ['classic', 'classic', 'classic', 'premium']
    items.forEach(item => checkout.addItem(item))
    expect(checkout.total()).toEqual('934.97')
  })
})
