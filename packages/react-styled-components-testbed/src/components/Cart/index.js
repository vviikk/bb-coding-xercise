import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
// eslint-disable-next-line
import { Badge,
  Button,
  ButtonClear,
  ButtonOutlined,
  DropdownDiv,
  DropdownWrapper,
  Icons,
} from '../../utils/Style'
import Checkout from '../../utils/Checkout' // no time to fix eslint import rules

const CartIcon = Icons.Cart
const { CaretUp, CaretDown } = Icons

const CartWrapper = styled.div`
  display: inline-block;
  margin: .5rem;
  visibility: ;


  asd:
`

export default class Cart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: this.props.items,
      total: 0,
      checkout: this.props.checkout,
      open: false,
    }
    this.toggleCart = this.toggleCart.bind(this)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      ...nextProps,
      items: nextProps.checkout.items,
      open: (prevState.open && !nextProps.checkout.items.length) ? false : prevState.open,
      total: nextProps.checkout.total(),
      itemCount: nextProps.checkout.getItemCount(),
    }
  }

  toggleCart() {
    this.setState(this.state.items.length ? {
      open: !this.state.open,
    } : { open: false })
  }


  done() {
    global.alert('I think that\'s about enough for an interview test!')
  }

  render() {
    return (
      <CartWrapper>
        <DropdownWrapper>
          <ButtonOutlined onClick={this.toggleCart}>
            {this.state.items.length ? <Badge>{`${this.state.items.length}`}</Badge> : ''}
            <CartIcon/>
            <strong>${this.state.total}</strong>
            {!this.state.open ? <CaretDown/> : (this.state.items.length && <CaretUp/>)}
          </ButtonOutlined>
          {this.state.open
            ? <DropdownDiv>
              { Object.keys(this.state.itemCount)
                .map(key => <div key={key}>{this.state.itemCount[key]}x {key}</div>)
              }
              <ButtonClear onClick={this.props.onCleanCart}>Clear items</ButtonClear>
              <Button onClick={this.done}>Checkout</Button>
            </DropdownDiv>
            : ''
          }
        </DropdownWrapper>
      </CartWrapper>
    )
  }
}

Cart.propTypes = {
  items: PropTypes.array,
  cartUpdated: PropTypes.array,
  user: PropTypes.object,
  checkout: PropTypes.object,
  onCleanCart: PropTypes.func,
}

Cart.defaultProps = {
  items: [],
  user: { name: 'default' },
  checkout: new Checkout(),
}
