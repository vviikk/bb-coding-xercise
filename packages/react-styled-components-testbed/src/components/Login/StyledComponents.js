import styled from 'styled-components'

const fieldHeight = 2.5

export const Label = styled.label`
  height: ${fieldHeight}rem;
`

export const LoginWrapper = styled.div`
  display: flex;
  max-width: 20rem;
  margin: 0 auto;
  flex-direction: column;
  > {
    display: block;
  }
`
