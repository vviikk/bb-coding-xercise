/* eslint-disable sort-imports */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Select, Button } from '../../utils/Style' // eslint-disable-line sort-imports

import { LoginWrapper, Label } from './StyledComponents'

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = { user: this.props.users[0] }
    this.setUser = this.setUser.bind(this)
  }

  setUser(event) {
    this.setState({
      user: this.props.users[event.target.value],
    })
  }

  render() {
    return (
      <LoginWrapper>
        <h3>Login</h3>
        <Label htmlFor='user'>Pick a company</Label>
        <Select onChange={this.setUser}>
          {
            this.props.users
              .map((user, index) => <option key={user.username} value={index}>{user.name}</option>)
          }
        </Select>
        <br/>
        <Button onClick={this.props.onLogin(this.state.user)}>Login</Button>
      </LoginWrapper>
    )
  }
}

Login.propTypes = {
  users: PropTypes.array,
  onLogin: PropTypes.func,
}

Login.defaultProps = {
  users: [],
}
