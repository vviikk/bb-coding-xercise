# A simple login & checkout using react, css-variables & styled-components ![''][travis-status][![Codacy Badge](https://api.codacy.com/project/badge/Grade/981b5a551f3d4272815fe009996539ae)](https://www.codacy.com/project/piggyslasher/awesome-checkout/dashboard?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=piggyslasher/awesome-checkout&amp;utm_campaign=Badge_Grade_Dashboard) 

## *done as an interview screening exercise*

## Technologies

- styled-components & css-variables
- CSS mostly ripped from [Milligram - A minimalist CSS framework.](https://milligram.io/)
- React without Redux
- Jest
- TravisCI build

## Calculations

All calculations are done in the [mock api](https://github.com/piggyslasher/awesome-checkout/blob/master/src/api/index.js) folder and have detailed tests.

All per-item price calculations are done by the [price](https://github.com/piggyslasher/awesome-checkout/tree/master/src/api/price) class and [tests](https://github.com/piggyslasher/awesome-checkout/blob/master/src/api/price/Price.test.js) are written extensively.

No routing implemented due to lack of time.

## Running the app

**Clone the repo**
Then:

```shell
cd awesome-checkout
yarn # install the deps
yarn start # starts the react app on port 3000

# or just run the test
yarn test
```


[travis-status]: https://api.travis-ci.org/piggyslasher/robot42.svg
