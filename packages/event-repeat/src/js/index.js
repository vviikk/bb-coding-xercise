console.clear()
/*
  Recurring Date Calculation
  An event takes place twice weekly on a Wednesday and a Saturday at 8pm.
  Write a function that calculates and returns the next valid draw date based
    on the current date and time and also on an optional supplied date.
    */

// https://hackernoon.com/a-quick-handbook-for-dates-in-javascript-7b71d0ef8e53

const cloneDate = (d, setCurTime) => {
  const newDate = new Date(d)
  if (setCurTime) {
    const now = new Date()
    newDate.setHours(now.getHours())
    newDate.setMinutes(now.getMinutes())
    newDate.setSeconds(now.getSeconds())
  }
  return newDate
}

export default (startDate, occursOn, maxWeeks = 10) => {
  let tempDate = cloneDate(startDate)
  const start = cloneDate(startDate, true)

  // Avoid UTC issues


  // piggyback off javascript Date API, nothing fancy
  const nextDayOfWeekFromDate = (d, dow, week) => {
    const newDate = new Date(d)
    newDate.setDate(d.getDate() + (dow + (week * 7 - d.getDay())))
    d.setDate(d.getDate() + (dow + (7 - d.getDay())) % 7)
    return newDate
  }

  let dateFound = false

  const nextDay = week => occursOn.sort().find((day) => {
    tempDate = nextDayOfWeekFromDate(tempDate, day, week)
    if (start.getTime() < tempDate.getTime()) {
      dateFound = tempDate
      return true
    }
    return false
  })

  // Don't like this. The other way is the iterace and reduce
  // and maybe use the modulo to do a quick check, but that would have a lot of useless
  // loops
  // Anyway, I think since I'm only using days given in the query, this is very fast.
  let currentWeek = 0
  while (!dateFound && currentWeek < maxWeeks) {
    nextDay(currentWeek++)
  }
  return dateFound
}
