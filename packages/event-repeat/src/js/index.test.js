const getNextEventDate = require('./index')

const getIndexedObj = arr => arr.reduce(
  (acc, c, idx) => ({ ...acc, ...({ [c.toLowerCase()]: idx }) }),
  {}
)
const namesOfDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
const DAYS = getIndexedObj(namesOfDays)

// Assertion below
describe('get next scheduled event', () => {
  const tuesday = new Date('23 Oct 2018 20:00')
  const wednesday = new Date('24 Oct 2018 20:00')
  const thursday = new Date('25 Oct 2018 20:00')
  const saturday = new Date('27 Oct 2018 20:00')
  const sunday = new Date('28 Oct 2018 20:00')
  const nextWednesday = new Date('31 Oct 2018 20:00')

  const occursOn = [DAYS.wednesday, DAYS.saturday]

  it('should return the same day if event occurs on that day', () => {
    expect(
      getNextEventDate(tuesday, [DAYS.tuesday])
    ).toEqual(tuesday)
    const dateToTest = new Date()
  })

  it('should return saturday start date is thursday', () => {
    expect(getNextEventDate(thursday, occursOn)).toEqual(saturday)
  })

  it('should return next wednesday if start date is sunday', () => {
    expect(getNextEventDate(sunday, occursOn)).toEqual(nextWednesday)
  })
})
